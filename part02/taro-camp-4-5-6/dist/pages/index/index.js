
"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _index = _interopRequireWildcard(require("../../npm/react/index.js"));

var _components = require("../../npm/components.js");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

class Index extends _index.Component {
  constructor() {
    super();

    _defineProperty(this, "onAddClick", () => {
      this.setState({
        count: this.state.count + 1
      });
    });

    _defineProperty(this, "onReduceClick", () => {
      this.setState({
        count: this.state.count - 1
      });
    });

    this.state = {
      count: 0
    };
  }

  componentDidMount() {
    console.log("执行componentDidMount");
    this.setState({
      count: 1
    });
  }

  render() {
    const text = this.state.count % 2 === 0 ? "偶数" : "奇数";
    return /*#__PURE__*/_index.default.createElement(_components.View, {
      class: "container"
    }, /*#__PURE__*/_index.default.createElement(_components.View, {
      class: "conut"
    }, /*#__PURE__*/_index.default.createElement(_components.Text, null, "count: ", this.state.count)), /*#__PURE__*/_index.default.createElement(_components.View, null, /*#__PURE__*/_index.default.createElement(_components.Text, {
      class: "text"
    }, text)), /*#__PURE__*/_index.default.createElement(_components.Button, {
      bindtap: this.onAddClick,
      class: "btn"
    }, "+1"), /*#__PURE__*/_index.default.createElement(_components.Button, {
      bindtap: this.onReduceClick,
      class: "btn"
    }, "-1"));
  }

}

exports.default = Index;    
Page(require('../../npm/app.js').createPage(Index))
    