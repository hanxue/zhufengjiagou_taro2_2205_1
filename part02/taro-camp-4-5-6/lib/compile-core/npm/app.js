"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Component = void 0;
exports.createPage = createPage;
exports.update = update;

var _container = _interopRequireDefault(require("./container"));

var _render = _interopRequireDefault(require("./render"));

var _react = _interopRequireDefault(require("react"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

class Component {
  constructor() {
    this.state = {};
  }

  setState(state) {
    update(this.$scope.$component, state);
    console.log(state, '调用');
  }

  _init(miniInstance) {
    //console.log('挂载小程序实列到React组件的$cope和$miniInstance上', miniInstance);
    this.$scope = miniInstance; // 在react组件实例上定义一个$scope属性，用来存储小程序的实列对象

    this.$miniInstance = miniInstance; // 在react组件实例上定义一个$scope属性，用来存储小程序的实列对象
  }

}

exports.Component = Component;

function createPage(component) {
  const config = {
    data: {
      root: {
        children: []
      }
    },

    onLoad() {
      this.container = new _container.default(this, "root");

      const pageElement = /*#__PURE__*/_react.default.createElement(component, {
        page: this
      });

      (0, _render.default)(pageElement, this.container);
    }

  };
  return config;
}

;

function update($component, state = {}) {
  $component.state = Object.assign($component.state, state);
  let data = $component.createData(state);
  data["$taroCompReady"] = true;
  $component.state = data;
  $component.$scope.setData(data);
}