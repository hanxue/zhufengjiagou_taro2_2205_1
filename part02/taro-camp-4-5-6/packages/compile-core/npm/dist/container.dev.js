"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _VNode = _interopRequireDefault(require("./VNode"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

var id = 0;

function generate() {
  return id++;
}

var Container =
/*#__PURE__*/
function () {
  function Container(context) {
    var rootKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "root";

    _classCallCheck(this, Container);

    this.context = context;
    this.root = new _VNode["default"]({
      id: generate(),
      type: "root",
      container: this
    });
    this.rootKey = rootKey;
  }

  _createClass(Container, [{
    key: "toJson",
    value: function toJson(nodes, data) {
      var _this = this;

      var json = data || [];
      nodes.forEach(function (node) {
        var nodeData = {
          type: node.type,
          props: node.props || {},
          text: node.text,
          id: node.id,
          children: []
        };

        if (node.children) {
          _this.toJson(node.children, nodeData.children);
        }

        json.push(nodeData);
      });
      return json;
    }
  }, {
    key: "applyUpdate",
    value: function applyUpdate() {
      var root = this.toJson([this.root])[0];
      this.context.setData({
        root: root
      });
    }
  }, {
    key: "createCallback",
    value: function createCallback(name, fn) {
      this.context[name] = fn;
    }
  }, {
    key: "appendChild",
    value: function appendChild(child) {
      this.root.appendChild(child);
    }
  }, {
    key: "removeChild",
    value: function removeChild(child) {
      this.root.removeChild(child);
    }
  }, {
    key: "insertBefore",
    value: function insertBefore(child, beforeChild) {
      this.root.insertBefore(child, beforeChild);
    }
  }]);

  return Container;
}();

exports["default"] = Container;