import Container from "./container";
import render from "./render";
import React from "react";

export class Component {
    constructor() {
        this.state = {};
    }
    setState(state) {
        update(this.$scope.$component, state);
        console.log(state, '调用');
    }
    _init(miniInstance) {
        //console.log('挂载小程序实列到React组件的$cope和$miniInstance上', miniInstance);
        this.$scope = miniInstance; // 在react组件实例上定义一个$scope属性，用来存储小程序的实列对象
        this.$miniInstance = miniInstance; // 在react组件实例上定义一个$scope属性，用来存储小程序的实列对象
    }
}

export function createPage(component) {
    const config = {
        data: {
            root: {
                children: [],
            },
        },
        onLoad() {
            this.container = new Container(this, "root");
            const pageElement = React.createElement(component, {
                page: this,
            });
            render(pageElement, this.container);
        },
    };
    return config;
};

export function update($component, state = {}) {
    $component.state = Object.assign($component.state, state);
    let data = $component.createData(state);
    data["$taroCompReady"] = true;
    $component.state = data;
    $component.$scope.setData(data);
}

