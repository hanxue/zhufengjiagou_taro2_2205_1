import { Component, createPage } from '../../npm/app';

class Index extends Component {
  constructor() {
    super();
    this.state = {
      count: 100,
      text: '123'
    };
  }
  componentDidMount() {
    console.log('执行componentDidMount');
    this.setState({
      count: 1
    });
  }
  createData() {
    this.__state = arguments[0];
    const text = this.state.count % 2 === 0 ? '偶数' : '奇数';
    Object.assign(this.__state, {
      text: text
    });
    return this.__state;
  }
  onAddClick() {
    this.setState({
      count: this.state.count + 1
    });
  }
  onReduceClick() {
    this.setState({
      count: this.state.count - 1
    });
  }
  onDoubleClick() {
    this.setState({
      count: this.state.count * 2
    })
  }
}
Index.$$events = ['onAddClick', 'onReduceClick', 'onDoubleClick'];

const options = createPage(Index);
Page(options);