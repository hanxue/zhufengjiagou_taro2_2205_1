/**
 * npm拷贝到输出目录
 */
async function copyNpmToWx() { }
/**
 *  页面生成4种输出到输出目录
 */
async function buildPages() { }
/**
 *  输出入口文件app.js与app.json
 */
async function buildEntry() { }
/**
 *  输出project.config.json
 */
async function buildProjectConfig() { }
/**
 *  检查目录等准备工作
 */
async function init() { }

async function main() {
    // 检查目录等准备工作
    await init()
    // npm拷贝到输出目录
    await copyNpmToWx();
    // 页面生成4种输出到输出目录
    await buildPages();
    // 输出入口文件app.js与app.json
    await buildEntry();
    // 输出project.config.json
    await buildProjectConfig();
}
main();