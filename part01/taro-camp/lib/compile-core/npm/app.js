"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.Component = void 0;
exports.createPage = createPage;
exports.update = update;

class Component {
  constructor() {
    this.state = {};
  }

  setState(state) {
    update(this.$scope.$component, state);
    console.log(state, '调用');
  }

  _init(miniInstance) {
    //console.log('挂载小程序实列到React组件的$cope和$miniInstance上', miniInstance);
    this.$scope = miniInstance; // 在react组件实例上定义一个$scope属性，用来存储小程序的实列对象

    this.$miniInstance = miniInstance; // 在react组件实例上定义一个$scope属性，用来存储小程序的实列对象
  }

}

exports.Component = Component;

function createPage(ComponentClass) {
  const componentInstance = new ComponentClass(); //对组件进行实列化

  const initData = componentInstance.state; //获取组件上的状态树

  const options = {
    data: initData,

    //定义小程序onLoad的生命周期函数
    onLoad() {
      // 这里this表示的小程序Page的实列
      this.$component = new ComponentClass();

      this.$component._init(this);
    },

    //定义小程序onReady小程序的生命周期函数
    onReady() {
      if (typeof this.$component.componentDidMount === 'function') {
        this.$component.componentDidMount();
      }
    }

  };
  const events = ComponentClass['$$events']; //获取组件上绑定的用户自定义的事件

  if (events) {
    events.forEach(eventHandlerName => {
      if (options[eventHandlerName]) return;

      options[eventHandlerName] = function () {
        this.$component[eventHandlerName].call(this.$component);
      };
    });
  }

  return options;
}

function update($component, state = {}) {
  $component.state = Object.assign($component.state, state);
  let data = $component.createData(state);
  $component.state = data;
  $component.$scope.setData(data);
}