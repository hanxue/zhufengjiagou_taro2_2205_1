"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var path = require("path");
var fse = require("fs-extra");
var const_1 = require("./const");
var babel_1 = require("./common/babel");
var buildSinglePage_1 = require("./common/buildSinglePage");
// npm拷贝到输出目录
function copyNpmToWx() {
    return __awaiter(this, void 0, void 0, function () {
        var npmPath, allFiles;
        var _this = this;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    npmPath = path.resolve(__dirname, "./npm");
                    return [4 /*yield*/, fse.readdirSync(npmPath)];
                case 1:
                    allFiles = _a.sent();
                    allFiles.forEach(function (fileName) { return __awaiter(_this, void 0, void 0, function () {
                        var fileContent, outputNpmPath, resCode;
                        return __generator(this, function (_a) {
                            switch (_a.label) {
                                case 0:
                                    fileContent = fse
                                        .readFileSync(path.join(npmPath, fileName))
                                        .toString();
                                    outputNpmPath = path.join(const_1.outputDir, "./npm/" + fileName);
                                    return [4 /*yield*/, babel_1["default"](fileContent, outputNpmPath)];
                                case 1:
                                    resCode = _a.sent();
                                    fse.ensureDirSync(path.dirname(outputNpmPath));
                                    fse.writeFileSync(outputNpmPath, resCode.code);
                                    return [2 /*return*/];
                            }
                        });
                    }); });
                    return [2 /*return*/];
            }
        });
    });
}
// 页面生成4种输出到输出目录
function buildPages() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            const_1.config.pages.forEach(function (page) {
                buildSinglePage_1.buildSinglePage(page);
            });
            return [2 /*return*/];
        });
    });
}
// 输出入口文件app.js与app.json
function buildEntry() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            fse.writeFileSync(path.join(const_1.outputDir, "./app.js"), "App({})");
            fse.writeFileSync(path.join(const_1.outputDir, "./app.json"), JSON.stringify(const_1.config, undefined, 2));
            return [2 /*return*/];
        });
    });
}
// 输出project.config.json
function buildProjectConfig() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            fse.writeFileSync(path.join(const_1.outputDir, "project.config.json"), "{\n    \"miniprogramRoot\": \"./\",\n    \"projectname\": \"app\",\n    \"description\": \"app\",\n    \"appid\": \"touristappid\",\n    \"setting\": {\n        \"urlCheck\": true,\n        \"es6\": false,\n        \"postcss\": false,\n        \"minified\": false\n    },\n    \"compileType\": \"miniprogram\"\n}\n        ");
            return [2 /*return*/];
        });
    });
}
// 检查目录等准备工作
function init() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            fse.removeSync(const_1.outputDir);
            fse.ensureDirSync(const_1.outputDir);
            return [2 /*return*/];
        });
    });
}
// 主函数
function main() {
    return __awaiter(this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: 
                // 检查目录等准备工作
                return [4 /*yield*/, init()];
                case 1:
                    // 检查目录等准备工作
                    _a.sent();
                    // npm拷贝到输出目录
                    return [4 /*yield*/, copyNpmToWx()];
                case 2:
                    // npm拷贝到输出目录
                    _a.sent();
                    // 页面生成4种输出到输出目录
                    return [4 /*yield*/, buildPages()];
                case 3:
                    // 页面生成4种输出到输出目录
                    _a.sent();
                    // 输出入口文件app.js与app.json
                    return [4 /*yield*/, buildEntry()];
                case 4:
                    // 输出入口文件app.js与app.json
                    _a.sent();
                    // 输出project.config.json
                    return [4 /*yield*/, buildProjectConfig()];
                case 5:
                    // 输出project.config.json
                    _a.sent();
                    return [2 /*return*/];
            }
        });
    });
}
debugger;
main();
